let FAClient = null;
let recordId = null;
let totalAmount = null;
let subtotalFormatted = null;
let subtotalNum = null;
// aHR0cDovL2xvY2FsaG9zdDo1MDAw localhost:5000
// aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL2NoZWNrb3V0

const SERVICE = {
    name: 'FreeAgentService',
    appletId: `aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL2NoZWNrb3V0`,
};

let quoteInfo = {
    entity: "quote",
    id: "",
    field_values: {}
};

function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        console.log(e)
    }
};

function startupService() {
    FAClient = new FAAppletClient({
        appletId: SERVICE.appletId,
    });

    FAClient.on("openPayment", (data) => {
        //document.querySelector('#order-list').innerHTML = '';
        console.log(data);
        recordId = data.id;
        subtotalFormatted = data.field_values.quote_field15.formatted_value;
        subtotalNum = data.field_values.quote_field15.value;
        document.getElementById('order-number').innerText = `#${data.seq_id}`;
        FAClient.open();
        FAClient.listEntityValues(
            {
                entity: "quote_line",
                filters: [
                    {
                        field_name: "parent_entity_reference_id",
                        operator: "includes",
                        values: [recordId],
                    },
                ],
            },
            (lines) => {
                console.log(lines);
                processLines(lines);
            });

          function processLines(lines) {
              console.log(lines);
              let list = document.getElementById('order-list');
              let totalEl = document.querySelector('#total-price');
              let total = 0;
              console.log(list);
              list.innerHTML = '';
              lines.map(line => {
                  let fieldValues = line.field_values;
                  let priceField = fieldValues.quote_line_field2;
                  let qty = fieldValues.quote_line_field1.display_value || 1;
                  total += parseFloat(priceField.display_value) * qty;
                  let liEl = document.createElement('li');
                  let heather4 = document.createElement('h4');
                  let heather4Text = document.createTextNode(fieldValues.quote_line_field0.display_value);
                  heather4.appendChild(heather4Text);
                  let heather5 = document.createElement('h5');
                  let heather5Text = document.createTextNode(`${priceField.formatted_value} * ${qty}`);
                  heather5.appendChild(heather5Text);
                  liEl.appendChild(heather4);
                  liEl.appendChild(heather5);
                  list.appendChild(liEl);
              });
              totalEl.innerText = `${subtotalFormatted}`;
              totalAmount = total;
          }

    });
}


function savePayment() {
    let paymentPayload = {
        entity: "payment",
        field_values: { payment_field0 : subtotalNum, payment_field1: "Credit Card" , payment_field2:recordId }
    };
    FAClient.createEntity(paymentPayload, (data) => {
        console.log(data.entity_value.id)
        let paymentId = data.entity_value.id;
        let payload = {
            entity: "charge",
            field_values: { charge_field7: "Processed", charge_field3: "", parent_entity_reference_id: "" }
        };
        payload.field_values.parent_entity_reference_id = recordId;
        payload.field_values.charge_field3 = paymentId;
        FAClient.createEntity(payload, (data) => {
            console.log(payload);
            //FAClient.navigateTo(`/payment/view/${paymentId}`);
        })
    });

}
